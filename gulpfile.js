'use strict'
var gulp = require('gulp');
var connect = require('gulp-connect');
var swPrecache = require('sw-precache');
var path = require('path');
var gutil = require('gulp-util');
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var nestedcss = require('postcss-nested');
var postCssImport = require('postcss-import');
var cssNano = require('cssnano');
var rename = require('gulp-rename');
var webpack = require('webpack-stream');

var PUBLIC_DIR = './public';
var APP_DIR = './app';

var buildNumber = new Date().getTime();
var packageJson = require('./package.json');

function getVersionWithBuildNumber() {
    return packageJson.version;
    //return packageJson.version + '+' + buildNumber;
}

gulp.task('webserver', ['generate-service-worker'], function () {
    connect.server({
        port: 8081,
        host: 'localhost',
        root: PUBLIC_DIR,
        livereload: true,
        https: false
    });
});

gulp.task('static-resources', () => {
    return gulp.src(['./resources/**/*'])
        .pipe(gulp.dest(PUBLIC_DIR))
})

gulp.task('post-css', ['css'], () => {
    return gulp.src('build/*.{css,css.map}')
        .pipe(gulp.dest(PUBLIC_DIR + '/styles'));
});

gulp.task('css', function () {
    var processors = [
        postCssImport,
        nestedcss,
        autoprefixer({browsers: ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4']}),
        cssNano,
    ];
    return gulp.src('app/styles/**/*.css')
        .pipe(sourcemaps.init())
        .pipe(postcss(processors))
        .pipe(rename('styles.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('build/'));
});

gulp.task('generate-service-worker', ['static-resources', 'post-css', 'js'], function (callback) {
    swPrecache.write(`${PUBLIC_DIR}/sw.js`, {
        cacheId: packageJson.name + '_' + getVersionWithBuildNumber(),
        directoryIndex: false,
        logger: gutil.log,
        navigateFallback: 'index.html',
        verbose: true,
        staticFileGlobs: [PUBLIC_DIR + '/**/*.{js,json,html,css,png,jpg,gif,svg,eot,ttf,woff}'],
        stripPrefix: PUBLIC_DIR
    }, callback);
});

gulp.task('js', () => {
    return gulp.src('app/lib/index.jsx')
        .pipe(webpack( require('./webpack.config.js') ))
        .pipe(rename('app.js'))
        .pipe(gulp.dest(PUBLIC_DIR + '/scripts'));
})

gulp.task('serve', ['generate-service-worker', 'webserver']);
gulp.task('build', ['generate-service-worker']);