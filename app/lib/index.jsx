'use strict';
import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

injectTapEventPlugin();

export default class Application extends React.Component {

    constructor(props) {
        super(props);

        this.state = {}
    }

    componentWillUnmount() {
        //
    }

    componentDidMount() {
        this.checkWebp();
    }

    checkWebp() {
        var img = new Image();
        img.onload = () => {
            var webp = !!(img.height > 0 && img.width > 0);
            this.setState({webp: webp});
            //TODO: store to local repo
        };
        img.onerror = () => {
            this.state.webp = false
            this.setState(this.state);
            this.loadGalleryData();
        };
        img.src = 'data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAAwA0JaQAA3AA/vuUAAA=';
    }

    render() {
        if (this.state.webp !== undefined) {
            return (
                <MuiThemeProvider>
                    <div>Hello app4!{this.getVersion()}</div>
                </MuiThemeProvider>
            );
        } else { //TODO: add fancy loading
            return <div>Loading...</div>
        }
    }

    getVersion() {
        return window['settings'].version;
    }
}

ReactDOM.render(<Application/>, document.getElementById('app_body'));