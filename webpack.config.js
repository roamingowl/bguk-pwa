var webpack = require('webpack');
var path = require('path');


var BUILD_DIR = path.resolve(__dirname, 'public');
var APP_DIR = path.resolve(__dirname, 'app/');

var getDEvTool = function () {
    if (process.env.NODE_ENV === 'production') {
        return 'cheap-module-source-map';
    } else {
        return 'eval';
    }
}

var config = {
    devtool: getDEvTool(),
    entry: [
        APP_DIR + '/lib/index.jsx',
    ],
    output: {
        path: BUILD_DIR,
        filename: 'index.jsx'
    },
    module: {
        loaders: [
            {
                test: /\.(jpg|png|gif|svg)$/i,
                loaders: [
                    'file?hash=sha512&digest=hex&name=img/[name].[ext]',
                    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            },
            {
                test: /\.js?/,
                include: APP_DIR,
                loader: 'babel-loader',
                query: {
                    presets: ["es2015", "react", "stage-0"]
                }
            }
        ],
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.optimize.UglifyJsPlugin({compress: {
            warnings: false
        }, minimize: process.env.NODE_ENV === 'production'}),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        })
    ],
    devServer: {
        inline: true,
        watch: true,
        https: false,
        colors: true,
        contentBase: './public',
        watchOptions: {
            poll: true
        },
    }
};

module.exports = config;
